//*----------------------------------------------------------------------------
//*         ATMEL Microcontroller Software Support  -  ROUSSET  -
//*----------------------------------------------------------------------------
//* The software is delivered "AS IS" without warranty or condition of any
//* kind, either express, implied or statutory. This includes without
//* limitation any warranty or condition with respect to merchantability or
//* fitness for any particular purpose, or against the infringements of
//* intellectual property rights of others.
//*----------------------------------------------------------------------------
//* File Name           : dataflash.c
//* Object              : High level functions for the dataflash
//* Creation            : HIi   10/10/2003
//*----------------------------------------------------------------------------

#include "dataflash.h"
#include "com.h"
void AT91F_InitSdram(void);



AT91S_DATAFLASH_INFO dataflash_info[CFG_MAX_DATAFLASH_BANKS];
static AT91S_DataFlash DataFlashInst;

int cs[][CFG_MAX_DATAFLASH_BANKS] = {
	{CFG_DATAFLASH_LOGIC_ADDR_CS0, 0},	/* Logical adress, CS */
	{CFG_DATAFLASH_LOGIC_ADDR_CS1, 1},	/* Logical adress, CS */
	{CFG_DATAFLASH_LOGIC_ADDR_CS2, 2},	/* Logical adress, CS */
	{CFG_DATAFLASH_LOGIC_ADDR_CS3, 3}
};

#define SIZE_REGS_64M     0x2188c159
#define SIZE_REGS_128M     0x2188c15a


int AT91F_DataflashInit (void)
{
	int i;
	int dfcode;

	AT91F_SpiInit ();

	for (i = 0; i < CFG_MAX_DATAFLASH_BANKS; i++) {
		dataflash_info[i].Desc.state = IDLE;
		dataflash_info[i].id = 0;
		dataflash_info[i].Device.pages_number = 0;
		dfcode = AT91F_DataflashProbe(cs[i][1], &dataflash_info[i].Desc);

		switch (dfcode) {
#if 0
		case AT45DB161:
			printf("\n\rflash nb %d  found",i);
			dataflash_info[i].Device.pages_number = 4096;
			dataflash_info[i].Device.pages_size = 528;
			dataflash_info[i].Device.page_offset = 10;
			dataflash_info[i].Device.byte_mask = 0x300;
			dataflash_info[i].Device.cs = cs[i][1];
			dataflash_info[i].Desc.DataFlash_state = IDLE;
			dataflash_info[i].logical_address = cs[i][0];
			dataflash_info[i].id = dfcode;
			break;

		case AT45DB321:
			printf("\n\rflash nb %d  found",i);
			dataflash_info[i].Device.pages_number = 8192;
			dataflash_info[i].Device.pages_size = 528;
			dataflash_info[i].Device.page_offset = 10;
			dataflash_info[i].Device.byte_mask = 0x300;
			dataflash_info[i].Device.cs = cs[i][1];
			dataflash_info[i].Desc.DataFlash_state = IDLE;
			dataflash_info[i].logical_address = cs[i][0];
			dataflash_info[i].id = dfcode;
			break;
#endif

		case AT45DB642:
			printf("\n\rflash nb %d  found",i);
			dataflash_info[i].Device.pages_number = 8192;
			dataflash_info[i].Device.pages_size = 1056;
			dataflash_info[i].Device.page_offset = 11;
			dataflash_info[i].Device.byte_mask = 0x700;
			dataflash_info[i].Device.cs = cs[i][1];
			dataflash_info[i].Desc.DataFlash_state = IDLE;
			dataflash_info[i].logical_address = cs[i][0];
			dataflash_info[i].id = dfcode;
			break;
#if 0
		case AT45DB128:
			printf("\n\rflash nb %d  found",i);
			dataflash_info[i].Device.pages_number = 16384;
			dataflash_info[i].Device.pages_size = 1056;
			dataflash_info[i].Device.page_offset = 11;
			dataflash_info[i].Device.byte_mask = 0x700;
			dataflash_info[i].Device.cs = cs[i][1];
			dataflash_info[i].Desc.DataFlash_state = IDLE;
			dataflash_info[i].logical_address = cs[i][0];
			dataflash_info[i].id = dfcode;
			break;
#endif
		default:
			printf("\n\rflash nb %d  not found",i);
			break;
		}
	}			
	printf("\n\r");
	AT91F_InitSdram();
	return (1);
}

int get_size_reg()
{
   if ((dataflash_info[2].id != 0)  && (dataflash_info[3].id != 0 )){
      return SIZE_REGS_128M;
   }
   else {
      return SIZE_REGS_64M;
   }
}

void AT91F_DataflashPrintInfo(void)
{
	int i;
        int *pregister;

	for (i = 0; i < CFG_MAX_DATAFLASH_BANKS; i++) {
		if (dataflash_info[i].id != 0) {
			printf ("Flash:");
			switch (dataflash_info[i].id) {
#ifndef  REDUCE_CODE_SIZE
			case AT45DB161:
				printf ("AT45DB161\n\r");
				break;

			case AT45DB321:
				printf ("AT45DB321\n\r");
				break;
#endif
			case AT45DB642:
				printf ("AT45DB642\n\r");
				break;
#ifndef  REDUCE_CODE_SIZE
			case AT45DB128:				
				printf ("AT45DB128\n\r");
				break;
#endif
			}
			printf ("Nb pages: %6d\n\r"
				"Page Sz: %6d\n\r"
				"Sz=%8d bytes\n\r"
				"Logical addr: 0x%08X\n\r",
				(unsigned int) dataflash_info[i].Device.pages_number,
				(unsigned int) dataflash_info[i].Device.pages_size,
				(unsigned int) dataflash_info[i].Device.pages_number *
				dataflash_info[i].Device.pages_size,
				(unsigned int) dataflash_info[i].logical_address);
		}
	}
}


/*------------------------------------------------------------------------------*/
/* Function Name       : AT91F_DataflashSelect 					*/
/* Object              : Select the correct device				*/
/*------------------------------------------------------------------------------*/
AT91PS_DataFlash AT91F_DataflashSelect (AT91PS_DataFlash pFlash,
										unsigned int *addr)
{
	char addr_valid = 0;
	int i;

	for (i = 0; i < CFG_MAX_DATAFLASH_BANKS; i++)
		if ((*addr & 0xFF000000) == dataflash_info[i].logical_address) {
			addr_valid = 1;
			break;
		}
	if (!addr_valid) {
		pFlash = (AT91PS_DataFlash) 0;
		return pFlash;
	}
	pFlash->pDataFlashDesc = &(dataflash_info[i].Desc);
	pFlash->pDevice = &(dataflash_info[i].Device);
	*addr -= dataflash_info[i].logical_address;
	return (pFlash);
}


/*------------------------------------------------------------------------------*/
/* Function Name       : addr_dataflash 					*/
/* Object              : Test if address is valid				*/
/*------------------------------------------------------------------------------*/
int addr_dataflash (unsigned long addr)
{
	int addr_valid = 0;
	int i;

	for (i = 0; i < CFG_MAX_DATAFLASH_BANKS; i++) {
		if ((((int) addr) & 0xFF000000) ==
			dataflash_info[i].logical_address) {
			addr_valid = 1;
			break;
		}
	}

	return addr_valid;
}

/*------------------------------------------------------------------------------*/
/* Function Name       : read_dataflash 					*/
/* Object              : dataflash memory read					*/
/*------------------------------------------------------------------------------*/
int read_dataflash (unsigned long addr, unsigned long size, char *result)
{
	unsigned int AddrToRead = addr;
        // AT91F_DataflashPrintInfo();
	AT91PS_DataFlash pFlash = &DataFlashInst;

	pFlash = AT91F_DataflashSelect (pFlash, &AddrToRead);
	if (pFlash == 0)
		return -1;

	return (AT91F_DataFlashRead (pFlash, AddrToRead, size, result));
}


/*-----------------------------------------------------------------------------*/
/* Function Name       : write_dataflash 				       */
/* Object              : write a block in dataflash			       */
/*-----------------------------------------------------------------------------*/
int write_dataflash (unsigned long addr_dest, unsigned int addr_src,
		     unsigned int size)
{
	unsigned int AddrToWrite = addr_dest;
	AT91PS_DataFlash pFlash = &DataFlashInst;

	pFlash = AT91F_DataflashSelect (pFlash, &AddrToWrite);
	if (AddrToWrite == -1)
		return -1;

	return AT91F_DataFlashWrite (pFlash, (unsigned char *) addr_src, AddrToWrite, size);
}


int erase_dataflash(int nb)
{
	AT91PS_DataFlash pFlash = &DataFlashInst;
	AT91S_DataFlashStatus status;
	int page_nb;

	if ( dataflash_info[nb].id != 0) {
		AT91F_SpiEnable(pFlash->pDevice->cs);

		pFlash->pDataFlashDesc = &(dataflash_info[nb].Desc);
		pFlash->pDevice = &(dataflash_info[nb].Device);
		for ( page_nb = 0 ; page_nb < dataflash_info[nb].Device.pages_number ; page_nb++) {
			if ( page_nb % 16 == 0)
				printf("Erase page nb %d\r\n",page_nb);
			status = AT91F_PageErase(pFlash,page_nb);
			AT91F_DataFlashWaitReady(pFlash->pDataFlashDesc, AT91C_DATAFLASH_TIMEOUT);
			if (!status) {
				printf("Erase page nb %d error\n",page_nb);
				return AT91C_DATAFLASH_ERROR;
			}
		}
	}
}




/*
 * Le sector protection register ( spr) contient la liste des secteurs de dataflash
 * a protéger.
 *
 * On efface le spr par la cmd
 *
 * Erase Sector Protection Register 3DH 2AH 7FH CFH
 *
 * On programme le spr par
 * 
 * Program Sector Protection Register 3DH 2AH 7FH FCH
 *
 * avec dans les bytes suivants la protection des secteurs.
 * Cette protection peut être activée par la command 
 *
 * Enable Sector Protection 3DH 2AH 7FH A9H
 *
 * Le conte,u du registre peut etre lu par 
 *
 * Read Sector Protection Register 32H xxH xxH xxH
 *
 * et désactivée par
 *
 * Disable Sector Protection 3DH 2AH 7FH 9AH
 * 
 */
void memset(unsigned char *ptr,char val , int size)
{
	int i;
	for ( i = 0 ; i < size ; i++) {
		*ptr = val;
		ptr++;
	}
}

static void clear_desc( AT91S_DataflashDesc *d)
{
  memset((unsigned char *)d,0,sizeof(AT91S_DataflashDesc));
}


AT91S_DataflashDesc desc;

t_read_lock_cmd read_lock_cmd_sector = 
{
	{
		0x32,0x00,0x00,0x00
	},
	32,
	32
};

t_read_lock_cmd read_lock_cmd_lockdown= 
{
	{
		0x35,0x00,0x00,0x00
	},
	32,
	32
};

t_read_lock_cmd  read_lock_cmd_security= 
{
	{
		0x77,0x00,0x00,0x00
	},
	128,
	128
};

void AT91F_SpiEnable(int cs);

void send_spi(int fl_nb)
{
  AT91PS_DataFlash pFlash = &DataFlashInst;
  if (dataflash_info[fl_nb].id != 0) {
    AT91F_SpiEnable(cs[fl_nb][1]);
    AT91F_SpiWrite(&desc);
    pFlash->pDataFlashDesc = &(dataflash_info[fl_nb].Desc);
    AT91F_DataFlashWaitReady(pFlash->pDataFlashDesc, AT91C_DATAFLASH_TIMEOUT);
  }
}

void read_lock_reg(t_read_lock_cmd *ptrcmd)
{
  int i;
  unsigned char rx[129];
  char *ptr;
  int fl_nb;
	
  for ( fl_nb = 0 ; fl_nb < CFG_MAX_DATAFLASH_BANKS ; fl_nb++) {
    
    clear_desc(&desc);
      
    memset(rx,0,sizeof(rx));
    rx[0] = 8;
    desc.tx_cmd_pt = ptrcmd->cmd;
    desc.rx_cmd_pt = rx;
    desc.tx_cmd_size = 4;
    desc.rx_cmd_size = 4;
    desc.tx_data_size = ptrcmd->txsize;
    desc.rx_data_size = ptrcmd->rxsize;
    desc.tx_data_pt = rx;
    desc.rx_data_pt = rx;
    send_spi(fl_nb);
    printf("\n\r");
    if (dataflash_info[fl_nb].id != 0) {
      if ( ptrcmd == &read_lock_cmd_sector ) {
	printf("Lock status reg ");
      }
      if ( ptrcmd == &read_lock_cmd_lockdown ) {
	printf("lockdown status reg ");
      }
      if ( ptrcmd == &read_lock_cmd_security ) {
	printf("security reg ");
      }
      for ( i = 0 ; i < ptrcmd->rxsize ; i++ )   {
	if ((i % 16) == 0) {
	  printf("\n\r");
	}
	printf("0x%02x:",rx[i]);
      }
      printf("\n\r");
    }
  }
}

unsigned char clear_lock_cmd[4]= 
{
  0x3D,0x2A,0x7F,0xCF
};


unsigned char lock_unlock_cmd[4]= 
  {
    0x3D,0x2A,0x7F,0xFC
  };

unsigned char enable_lock_unlock_cmd[4]= 
  {
    0x3D,0x2A,0x7F,0xA9
  };

unsigned char disable_lock_unlock_cmd[4]= 
  {
    0x3D,0x2A,0x7F,0x9A
  };

void flash_en_lock_unlock(int fl_nb,unsigned char *cmd)
{
  unsigned char rxf[4];
  unsigned char rx[33];
  clear_desc(&desc);
  desc.tx_cmd_pt = cmd;
  desc.rx_cmd_pt = rxf;
  desc.tx_cmd_size = 4;
  desc.rx_cmd_size = 4;
  send_spi(fl_nb);
}

void write_lock_reg(int fl_nb,int val)
{
  unsigned char rxf[4];
  unsigned char rx[33];
  memset(rx,val,sizeof(rx));
  clear_desc(&desc);
  desc.tx_cmd_pt = lock_unlock_cmd;
  desc.rx_cmd_pt = rxf;
  desc.tx_cmd_size = 4;
  desc.rx_cmd_size = 4;
  desc.tx_data_size = 32;
  desc.rx_data_size = 32;
  desc.tx_data_pt = rx;
  desc.rx_data_pt = rx;
  send_spi(fl_nb);
}

void erase_lock_reg(int fl_nb)
{
  unsigned char rx[5];
  memset(rx,0,sizeof(rx));
  clear_desc(&desc);
  desc.tx_cmd_pt = clear_lock_cmd;
  desc.rx_cmd_pt = rx;
  desc.tx_cmd_size = 4;
  desc.rx_cmd_size = 4;
  send_spi(fl_nb);
}

void set_lock_reg(int fl_nb)
{
  erase_lock_reg(fl_nb);
  write_lock_reg(fl_nb,0xff);
}

void clr_lock_reg(int fl_nb)
{
  erase_lock_reg(fl_nb);
  write_lock_reg(fl_nb,0);
}

void flash_enable_lock(int fl_nb)
{
  flash_en_lock_unlock(fl_nb,enable_lock_unlock_cmd);
}

void flash_disable_lock(int fl_nb)
{
  flash_en_lock_unlock(fl_nb,disable_lock_unlock_cmd);
}

